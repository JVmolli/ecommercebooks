import { BookType } from "../types/Book";
export const ApiCall = async ({
  method,
  title
}: {
  method: string;
  title: string;
}): Promise<Array<BookType>> => {
  const host = "http://openlibrary.org/search.json?q=";
  const books: any = await fetch(`${host}${title}`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      return data;
    });
  return Promise.all(
    books.docs.map((book: any) => ({
      title_suggest: book.title_suggest,
      cover_i: book.cover_i,
      author_name: book.author_name,
      first_publish_year: book.first_publish_year,
      subtitle: book.subtitle,
      isbn: book.isbn !== undefined ? book.isbn[0] : null
    })) as Array<BookType>
  );
};
