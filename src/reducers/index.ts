import { combineReducers } from "redux";
import { BooksReducer } from "./books";

const appReducers = combineReducers({
  BooksReducer
});

export default appReducers;
