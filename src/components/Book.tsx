/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import {
  IonCard,
  IonButton,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonRow,
  IonCol
} from "@ionic/react";
import "./Book.scss";
import { BookType } from "../types/Book";

const Book = ({ book }: { book: BookType }) => {
  return (
    <IonCard
      style={{ minWidth: 150, maxWidth: 200, height: 380 }}
      className={"animation"}
    >
      <IonRow>
        <IonCol size="1"></IonCol>
        <IonCol size="10">
          <img
            style={{ maxHeight: 200 }}
            src={`//covers.openlibrary.org/b/id/${book.cover_i}-M.jpg`}
          />
        </IonCol>
        <IonCol size="1"></IonCol>
      </IonRow>
      <IonRow>
        <IonCardHeader>
          <IonCardSubtitle>{book.title_suggest}</IonCardSubtitle>
          <IonCardSubtitle>{book.subtitle}</IonCardSubtitle>
          <IonCardSubtitle>{book.subtitle}</IonCardSubtitle>
        </IonCardHeader>
        <IonCardContent>
          <IonButton
            fill="outline"
            slot="start"
            onClick={() => console.log("adding " + book.isbn)}
          >
            +
          </IonButton>
        </IonCardContent>
      </IonRow>
    </IonCard>
  );
};

export default Book;
