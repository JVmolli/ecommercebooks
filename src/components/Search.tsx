import { IonSearchbar } from "@ionic/react";
import { connect } from "react-redux";
import { searchBooks } from "../actions/";
import { ApiCall } from "../services/callApi";
import React, { useState } from "react";
export const Search = ({ onSearch }: { onSearch: Function }) => {
  const [timeOut, setTime] = useState();
  const [statusTime, setStatusTime] = useState(true);
  const [valState, setVal] = useState<string>("");
  const hangdleSearch = async (val: string) => {
    if (statusTime) {
      if (val.length < 2) return;
      setTime(
        setTimeout(async () => {
          await ApiCall({ method: "GET", title: valState }).then(data => {
            onSearch(valState, data);
          });
          setStatusTime(true);
        }, 1500)
      );
      setStatusTime(false);
    } else {
      setVal(val);
    }
  };
  return (
    <IonSearchbar
      animated
      placeholder="Find your book"
      onIonInput={(val: any) => hangdleSearch(val.target.value)}
      onIonChange={(val: any) => hangdleSearch(val.target.value)}
      debounce={1000}
    ></IonSearchbar>
  );
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onSearch: (val: any, data: any) => {
      dispatch(searchBooks(val, data));
    }
  };
};

const SearchWithSearcher = connect(null, mapDispatchToProps)(Search);

export default SearchWithSearcher;
