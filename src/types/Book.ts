export interface BookType {
  title_suggest: string;
  cover_i: string;
  subtitle: string;
  first_publish_year: Number;
  author_name: string[];
  isbn: Number;
}
