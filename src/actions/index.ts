export const searchBooks = (title: string, data: any) => {
  return {
    type: "SEARCH_BOOKS",
    title,
    data
  };
};
