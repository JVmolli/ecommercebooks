import React, { useState, useEffect } from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonRow,
  IonCol,
  IonLoading
} from "@ionic/react";
import "./Home.scss";
import { ApiCall } from "../services/callApi";
import Book from "../components/Book";
import { BookType } from "../types/Book";
import { connect } from "react-redux";
import SearchWithSearcher from "../components/Search";

const Home: React.FC = (props: any) => {
  const [books, setBooks] = useState<Array<BookType>>([]);
  useEffect(() => {
    ApiCall({ method: "GET", title: "java" }).then(data =>
      setBooks(data.filter(book => book.cover_i !== undefined))
    );
  }, []);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Home</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <SearchWithSearcher />

        <IonLoading isOpen={true} message={"Please wait..."} duration={2000} />
        {props.books?.BooksReducer?.state?.data.length > 0 ||
        books.length > 0 ? (
          <IonRow>
            {(props.books?.BooksReducer?.state?.data || books)
              .filter((book: any) => book.cover_i !== undefined)
              .map((book: any, i: number) => {
                return (
                  <IonCol key={i}>
                    <Book book={book}></Book>
                  </IonCol>
                );
              })}
          </IonRow>
        ) : (
          <IonLoading isOpen={true} message={"Please wait..."} />
        )}
      </IonContent>
    </IonPage>
  );
};

const mapStateToProps = (state: any) => {
  return {
    books: state
  };
};
const HomeWithSearch = connect(mapStateToProps)(Home);

export default HomeWithSearch;
